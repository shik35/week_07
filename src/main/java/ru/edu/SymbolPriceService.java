package ru.edu;

import ru.edu.model.Symbol;

import java.util.List;
import java.util.UUID;

/**
 * Сервис для получения котировок, должен быть thread-safe.
 *
 */
public interface SymbolPriceService {

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    Symbol getPrice(String symbolName);
}
